---
layout: default
title: For external researchers
parent: Open MR Protocols
has_children: false
nav_order: 6
---






# User Guide for External Researchers
{: .fs-9 }

A guide for external researchers on how to use the Open WIN MR protocols database
{: .fs-6 .fw-300 }

---

## For external researchers
External researchers are able to search the database for MR protocols which research teams have chosen to make openly available. These may be deposited to support publications as supplementary methods material, or they may form the main body of research in sequence development.

![gif - external](../../../img/img-protocols/gif_external.gif)

### 1. Access the MR Protocols database
Available here: [http://open.win.ox.ac.uk/protocols/](http://open.win.ox.ac.uk/protocols/)

### 2. Search

Search the database using keyword terms based on anatomy, the imaging system, sequence types, or the theoretical application.

The search returns a **list of open access protocols** containing your search term, with additional information on the species of participant in the study, the project name as it appears on our resource booking system (Calpendo) and date of most recent changes to each entry.


### 3. Top level details

Select the protocol you are interested in to view more details. The name of the protocol you are viewing will be shown at the top, along with a button to download all the materials for the current version of this entry. Further top-level details are as below:

`Open Access Status` (this will only be `Open Access` for protocols visible to external researchers) and a stable URL for reference.

`Version Info` will contain the date the protocol was first entered, the current version, and a link to access the full version history.

`Reference list` will describe how the author would like to be cited in reuse of this material, and any other published materials (such as journal papers or software) which should be considered alongside this material. This may contain a weblink out to where the DOI was issued.

`Scanning Info` will give basic details about the hardware and application, including the total duration of acquisition time.

### 4. Protocol Info

Authors are advised to include the following information in the `Protocol Info` section:
- `Description`: This section should identify and highlight important features of the protocol. For example the study design, methods, and procedure.
- `Change log`: Brief details of changes to the protocol with each version update on the database
- `Usage guidance`: Including contact details for someone to discuss the protocol or its reuse (optional), how to cite the protocol and terms for reuse.
- `File Attachments`: The names of any files additionally attached to this entry (for example radiographer's procedure) will be listed to the top of this section.

### 5. Individual sequence details

The following sections give the name, keyword and researcher description and acquisition run time of each sequence in the protocol.


### 5. Download

At the top of the page, just below the protocol name is the download button. This will lead to a download link for a zip file that contains the information shown in the protocol overview, all attached files, plus full configuration details for each sequence (not shown online).

Please review the `Disclaimer and Usage` guidance at the top of the download.

## How To Cite A Protocol

Please refer to the authors citation guide contained within the `Reference List` section of each entry to appropriately acknowledge re-use of material.

## Contact

Information about who to contact regarding re-use of a specific protocol should be provided in the usage guidance. If not contact information is provided, please [contact us](https://open.win.ox.ac.uk/protocols/about).

## How To Create Your Own Protocols Database

The main source code for this database is openly available on [gitlab](https://git.fmrib.ox.ac.uk/acquisition/win-protocols-db).

If you reuse the code to build your own database, please cite it as...

Coming soon
{: .label .label-yellow }
