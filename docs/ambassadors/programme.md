---
layout: default
title: Programme schedule
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 6
---
# Programme schedule
{: .fs-9 }

Find out about how the Ambassadors programme will be structured and scheduled

{: .fs-6 .fw-300 }

---

**Contents**
- [Technology](#technology)
- [Call structure](#call-structure)
- [Shadowing](#shadowing)
- [Schedule](#schedule)


All activities of the Ambassadors programme will take place online. Any in-person celebratory or team building meetings will be designed to be as accessible as possible and accommodating to the responsibilities of our Ambassadors.

## Technology

We will use a number of online technologies to work efficiently, transparently and reproducibly. Many of these solutions will provide you with transferable skills which you will be able to incorporate into your own research workflow. *Training and support will be provided to ensure all Ambassadors are comfortable working in this way.*

Regular meeting calls will take place on MS Teams. For quick messaging we will also use Slack.

We will use [hackmd.io](http://hackmd.io/) to write collaborative notes during the call. You will need a [GitHub](http://github.com/) account to use hackmd. Feel free to take a look at the [notes from our previous meetings](https://git.fmrib.ox.ac.uk/open-science/community/open-win-ambassadors/-/tree/master/call-notes).

We will use the [WIN GitLab instance](http://git.fmrib.ox.ac.uk/) to write documentation and manage project work. You will require a WIN IT account to access GitLab.

We may use [padlet](https://padlet.com) for creative exercises.

## Group meetings structure

#### First weeks

During the first month of the programme you will participate in workshops to help you work with git and GitLab, understand more about the value and practices of open science, and learn how to use the Open WIN tools.

#### Weeks 5-8

Once you are familiar with the Open WIN infrastructure and working on GitLab, you will be encouraged to decide which tool(s) you would like to work on and develop further. You may also have ideas about which other projects you would like to take on or contribute to. We will allocate individual as well as group projects for you to work on during the rest of the programme, based on interests, preferences and what is needed by the wider community. 

#### Rest of the year

Our regular calls will be 30-60 minutes long; initially on a weekly basis and every other week later in the year.

These calls will mostly be used for updates on ongoing projects, including discussions, questions, opinions, brainstorming, support, guidance, feedback, etc. Occasionally we might also use the time for co-working on specific tasks and issues. 

You may choose to contribute outside of this time, or arrange co-working times with other ambassadors which better suite your schedules.

<!--
## Shadowing

In response to feedback from the 2021-2022 Ambassadors, we have incorporated various opportunities for you to shadow the work of our Community Engagement Coordinator, so you can learn about the operational aspects of doing open science and supporting the wider community. You will be invited to attend the termly [Open WIN Steering Group](../../community/community-who/#open-win-steering-group) meetings, join one-to-one support meetings with researchers seeking to incorporate open science practices in their work, and join a team inbox.
-->

## Launch and close meetings

In January 2024 you will join the Ambassadors of previous years for a formal introduction to the Open WIN (delivered by the [Open WIN Steering Group](../../community/community-who#open-win-steering-group), hear the results of our [programme evaluation](../goals/#evaluation-of-the-programme-and-your-experience) and share your expectations and wishes for the year ahead. These activities will be held at a College in January 2024, in an informal environment where we can celebrate each others success and contributions.

## Schedule

Below is a guideline schedule of activities for the 2024 cohort. Calendar invitations for each meeting will be sent to all Ambassadors.

We recognise that these activities will sit alongside your usual commitments. We will endeavour to be responsive to your work schedule.

| Call | Date | Time | Topic  |
|---|---|---|---|
| 1 | Tuesday 28th November 2023 | 09:30 - 11:00 | intro to the programme |
| 2-5 | Tuesdays  December 2023 | 09:30 - 10:30 | git and gitlab, tools, demos, etc. |
| 6-10 | Tuesdays January-February 2024 | 09:30 - 10:30 | setting up projects, planning, training, policy, incentives, etc. |
| throughout 2024 | Tuesdays 2024 | 09:30 - 10:30 | working on projects, updates, co-working |
| end 2024 | a Tuesday in November / December 2024 | 09:30 - 11:00 | close out and feedback |
